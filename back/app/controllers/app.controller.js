const express = require('express');
const router = express.Router();
const expressValidator = require('express-validator');

const STATUS = require('../_constants/status');
const dbContext = require('../services/db-context.service');

router.use(expressValidator());

router.get('/temps', (req, res) => {
    dbContext.find('temps', (err, items) => {
        if (err) return res.status(STATUS.SERVER_ERROR).send({error: 'server error'});
        if (!items) return res.status(STATUS.NOT_FOUND).send({error: 'temps not found'});

        res.status(STATUS.SUCCESS).send(items);
    });
});

router.get('/prec', (req, res) => {
    dbContext.find('prec', (err, items) => {
        if (err) return res.status(STATUS.SERVER_ERROR).send({error: 'server error'});
        if (!items) return res.status(STATUS.NOT_FOUND).send({error: 'prec not found'});

        res.status(STATUS.SUCCESS).send(items);
    });
});

module.exports = router;
