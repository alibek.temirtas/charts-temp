const statuses = {
  "SUCCESS": 200,
  "REQUIRE_PARAMS": 400,
  "REGISTERED": 401,
  "FORBIDDEN": 403,
  "NOT_FOUND": 404,
  "SERVER_ERROR": 500
};

module.exports = statuses;
