var MongoClient = require('mongodb').MongoClient;

var state = {
  db: null
};

exports.connect = (url, done) => {
  if (state.db) {
    return done();
  }

  MongoClient.connect(url, { useNewUrlParser: true }, (error, database) => {
    if (error) {
      return console.log(error);
    }
    state.db = database.db('test-app');
    done();
  });

};

exports.get = () => {
  return state.db;
};
