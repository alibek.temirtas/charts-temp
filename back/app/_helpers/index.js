const cors = require('./cors');

module.exports = {
  CORS: cors.makeAccessControl
};
