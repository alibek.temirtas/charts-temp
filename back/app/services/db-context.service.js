const ObjectID = require('mongodb').ObjectID;
const db = require('../db');
const moment = require('moment');

function insertOne(collection, data, cb) {
    db.get().collection(collection).insertOne(data, (err, result) => {
        cb(err, result);
    });
}

function find(collection, cb) {
    db.get().collection(collection).find().toArray((err, docs) => {
        cb(err, docs);
    });
}

function findOne(collection, id, cb) {
    db.get().collection(collection).findOne({_id: ObjectID(id)}, (err, result) => {
        cb(err, result);
    });
}

function findByUserId(collection, data, cb) {
    db.get().collection(collection).find({userId: ObjectID(data.userId)}).sort({_id: -1}).limit(35).toArray((err, docs) => {
        cb(err, docs);
    });
}

function count(collection, params = {}) {
    return db.get().collection(collection).find(params).count();
}


module.exports = {
    insertOne: insertOne,
    findOne: findOne,
    count: count,
    find: find
};
