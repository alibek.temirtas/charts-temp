const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const path = require('path');
const helpers = require('./_helpers');

// CONTROLLERS :
const appController = require('./controllers/app.controller');

const app = express();

global.__absolutePath = path.resolve('static');

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(cookieParser());

app.use(helpers.CORS);

app.get('/api', (req, res) => {
    res.send('test-app web api is running');
});

app.use('/api/app', appController);

module.exports = app;
