import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, OnDestroy} from '@angular/core';
// import {Chart} from 'chart.js';

import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {AppService} from "./services/app.service";

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent implements OnInit, OnDestroy {
  arr;
  dict: any = {};

  startYear;
  endYear;
  yearList = [];

  LinechartLabels = [];
  LinechartData = [];
  chartColors: Array<any> = [
    {
      backgroundColor: ['#d13537', '#b0o0b5']
    }
  ];

  private _onDestroy = new Subject();

  constructor(private _appService: AppService,
              private _cd: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.getData('temps');
  }

  ngOnDestroy(): void {
    this._onDestroy.next();
  }

  getData(type){
    this._appService.getData(type)
      .pipe(takeUntil(this._onDestroy))
      .subscribe((data: any) => {
        this.initData(data);
      }, error => {

      });
  }

  isLeapYear(year): boolean {
    return year % 400 == 0 || (year % 4 == 0 && year % 100 != 0);
  }

  initData(arr) {
    this.startYear = arr[0]['t'].substring(0, 4);
    this.endYear = arr[arr.length - 1]['t'].substring(0, 4);

    for (let j = this.startYear; j <= this.endYear; j++) {
      this.yearList.push(j);
    }

    for (let j = this.startYear; j <= this.endYear; j++) {
      this.dict[j] = 0;
    }

    for (let i = arr.length - 1; i >= 0; i--) {

      let year = arr[i]['t'].substring(0, 4);

      this.dict[year] += arr[i]['v'];
    }

    for (let key in this.dict) {
      this.dict[key] /= this.isLeapYear(key) ? 366 : 365;
    }

    this.generateChart();
  }

  generateChart() {
    this.LinechartLabels = [];
    this.LinechartData = [];

    for (let j = +this.startYear; j <= +this.endYear; j++) {
      this.LinechartLabels.push(j);
      this.LinechartData.push(this.dict[j]);
    }
  }
}
