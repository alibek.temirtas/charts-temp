import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

import {ConfigsEnv} from "../consts/configs";

@Injectable()
export class AppService {
  private _serverUrl = ConfigsEnv.UrlApi;

  constructor(private _http: HttpClient) {
  }


  getData(type: string = 'temps'): any {

    return this._http.get(`${this._serverUrl}/api/app/${type}`, {}).pipe(
      map((response: any) => {
        return response;
      })
    );
  }


}
